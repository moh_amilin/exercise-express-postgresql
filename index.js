const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

const db = require('./controller/userController')

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);

app.get('/', (req, res) => {
    res.json({info: 'Node js'});
});

app.listen(port,()=> {
    console.log(`Server running on port ${port}`);
});

app.get('/get/users', db.viewUsers);
app.get('/get/user/:id', db.viewUserById);
app.post('/add-users', db.createUser);
app.put('/update-user/:id', db.updateUser);
app.delete('/delete-user/:id', db.deleteUser);
