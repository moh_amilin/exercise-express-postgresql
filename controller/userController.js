const pool = require("../config/queries");

module.exports = {
  viewUsers: async (req, res) => {
    try {
      const user = await pool.query("SELECT * FROM users ORDER BY id ASC ");
      // console.log('data', allOrgs)
      res.status(200).json({ message: "success", data: user.rows });
    } catch (err) {
      console.error({ message: `${err}` });
    }
  },

  viewUserById: async (req, res) => {
   try {
    const id = parseInt(req.params.id);
    const user = await pool.query('SELECT * FROM users WHERE id = $1', [id])
    res.status(200).json({
        message: "success add data",
        data: user.rows,
      });
       
   } catch (error) {
    res.status(400).json({ message: `error is ${error}` });
   }
  },

  createUser: async (req, res) => {
    try {
      const { name, email } = req.body;
      const user = await pool.query(
        "INSERT INTO users (name, email) VALUES ($1, $2) RETURNING * ",
        [name, email]
      );
      console.log("data", user.rows);
      res.status(200).json({
        message: "success add data",
        data: user.rows,
      });
    } catch (error) {
      res.status(400).json({ message: `error is ${error}` });
    }
  },

  updateUser: async (req, res) => {
    try {
      const id = parseInt(req.params.id);
      const { name, email } = req.body;
      const user = await pool.query(
        "UPDATE users SET name = $1, email=$2 WHERE id = $3 RETURNING *",
        [name, email, id]
      );
      console.log("update", user);
      res.status(200).json({
        message: "Updated",
        data: user.rows,
      });
    } catch (error) {
      res.status(400).json({
        message: `Error is ${error}`,
      });
    }
  },

  deleteUser: async (req, res) => {
    try {
      const id = parseInt(req.params.id);
      await pool.query("DELETE FROM users WHERE id=$1", [id]);
      res.status(200).json({
        message: "deleted",
      });
    } catch (error) {
      res.status(400).json({
        message: `Error is ${error}`,
      });
    }
  },
};
