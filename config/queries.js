require('dotenv').config();
const Pool = require('pg').Pool;
// const pool = new Pool({
//     host: process.env.HOST,
//     database: process.env.DATABASE,
//     user: process.env.USER,
//     password: process.env.PASSWORD,
//     port: process.env.PORT
// });

const pool = new Pool({
    host: 'localhost',
    database: 'learn-api',
    user: 'amilin',
    password: 'root',
    port: 5432
});

module.exports = pool;